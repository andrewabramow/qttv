#include "mainwindow.h"
#include "./ui_window.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setChLogo(QLabel* label) {
    QString path = "D://QtProjects//QtTV//" + getChanNumb() + "ch.png";
    QPixmap pix(path);
    int w = label->width ();
    int h = label->height ();
    label->setPixmap (pix.scaled (w,h,Qt::KeepAspectRatio));
}
QString MainWindow::getVolume () {
    return  QString::number(volume);
}
QString MainWindow::getChanNumb() {
    return  QString::number(chanNumb);
}
void MainWindow::status() {
    lineEdit->setText("Ch: " + getChanNumb() + ", Volume: " + getVolume());
}
void MainWindow::ch0()
{
    if (power){
        chanNumb = 0;
        setChLogo(label);
        status();
    }
}
void MainWindow::ch1()
{
    if (power){
        chanNumb = 1;
        setChLogo(label);
        status();
    }
}
void MainWindow::ch2()
{
    if (power){
        chanNumb = 2;
        setChLogo(label);
        status();
    }
}
void MainWindow::ch3()
{
    if (power){
        chanNumb = 3;
        setChLogo(label);
        status();
    }
}
void MainWindow::ch4()
{
    if (power){
        chanNumb = 4;
        setChLogo(label);
        status();
    }
}
void MainWindow::ch5()
{
    if (power){
        chanNumb = 5;
        setChLogo(label);
        status();
    }
}
void MainWindow::ch6()
{
    if (power){
        chanNumb = 6;
        setChLogo(label);
        status();
    }
}
void MainWindow::ch7()
{
    if (power){
        chanNumb = 7;
        setChLogo(label);
        status();
    }
}
void MainWindow::ch8()
{
    if (power){
        chanNumb = 8;
        setChLogo(label);
        status();
    }
}
void MainWindow::ch9()
{
    if (power){
        chanNumb = 9;
        setChLogo(label);
        status();
    }
}
void MainWindow::prevCh()
{
    if (power) {
        chanNumb-=1;
        if (chanNumb<0) chanNumb = 10;
        setChLogo(label);
        status();
    }
}
void MainWindow::nextCh()
{
    if (power) {
        chanNumb+=1;
        if (chanNumb>10) chanNumb = 0;
        setChLogo(label);
        status();
    }
}
void MainWindow::onOff()
{
    if (!power) {
        power = true;
        status();
    }
    else {
        power = false;
        lineEdit->setText("OFF");
        chanNumb = 0;
        volume = 50;
        setChLogo(label);
    }
}
void MainWindow::mute()
{
    if (power) {
        if (volume == 0) volume = 50;
        else volume = 0;
        status();
    }

}
void MainWindow::plusVol()
{
    if (power) {
        volume += 10;
        if (volume > 100) volume = 100;
        status();
    }
}
void MainWindow::minVol()
{
    if (power) {
        volume -= 10;
        if (volume < 0) volume = 0;
        status();
    }
}


