#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QLabel>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QLineEdit* lineEdit=nullptr;
    QLabel* label=nullptr;
    QString getVolume();
    QString getChanNumb();
    void status();
    void setChLogo(QLabel* label);
    QLabel* getLabel ();

public slots:
    void ch1();
    void ch2();
    void ch3();
    void ch4();
    void ch5();
    void ch6();
    void ch7();
    void ch8();
    void ch9();
    void ch0();
    void prevCh();
    void nextCh();
    void onOff();
    void mute();
    void plusVol();
    void minVol();

private:
    Ui::MainWindow *ui;
    bool power = false;
    int volume = 50;
    int chanNumb = 0;
};
#endif // MAINWINDOW_H
